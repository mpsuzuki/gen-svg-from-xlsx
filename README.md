# countImgsPerCell.rb

a script to extract rowIndex, cellIndex, and mediaID from Drawing XML in XLSX.
```
$ ./countImgsPerCell.rb < xlsx-extracted-dir/xl/drawings/drawing1.xml
[1, 4, ["rId1"]]
[1, 5, ["rId863"]]
[2, 4, ["rId2"]]
[2, 5, ["rId864"]]
[3, 4, ["rId3"]]
[3, 5, ["rId865"]]
[4, 4, ["rId4"]]
[4, 5, ["rId866"]]
[5, 4, ["rId5"]]
[5, 5, ["rId867"]]
[6, 4, ["rId6"]]
[6, 5, ["rId868"]]
[7, 4, ["rId7"]]
...
```

# keepUniqueImgs.rb

a script to remove the duplicated images in single cell from Drawing XML in XLSX.
the option "--only-one" removes all images except of the last one, regardless with the mediaID.

```
$ ./keepUniqueImgs.rb < xlsx-extracted-dir/xl/drawings/drawing1.xml.orig > xlsx-extracted-dir/xl/drawings/drawing1.xml
```

# genSvg.rb

a script to generate the SVG for each cell including a raster image and the vector autoshapes.
```
$ ./genSvg.rb --dst-dir=svgs --insert-tag-before-rc --pick-tag-from-column=0 xlsx-extracted-dir/xl/drawings/drawing1.xml
[0, 0, "0", "seq"]
[0, 1, "4591", "CCZ location"]
[0, 2, "4593", "corresp. ucs"]
[0, 3, "4592", "N5105 code"]
[0, 4, "4579", "N5105"]
[0, 5, "4594", "CCZ"]
[0, 6, "4596", "tag"]
[0, 7, "4595", "type"]
[0, 8, "4597", "guwen?"]
[0, 9, "4598", "taboo info"]
[0, 10, "4600", "connection, overshoot"]
[0, 11, "4599", "struct"]
[0, 12, "4601", "reprint damage"]
[0, 13, "4210", "minor stroke"]
[0, 14, "4602", "to be discussed?"]
[0, 15, "2", "comment"]
[0, 16, "5083", "dup?"]
[1, 0, "08423:_"]
...
svgs/markedImg-seq08423-001-5.svg
svgs/markedImg-seq08472-002-5.svg
...
```

# genCompareHtml.rb

a script to generate flex-contained mini-tables for comparison, from tsv
