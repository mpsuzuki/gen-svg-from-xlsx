#!/usr/bin/env ruby
Encoding.default_internal = "utf-8"
Encoding.default_external = "utf-8"

require "nokogiri"
require "rmagick"
require "base64"
require "json"
require "./myGetOpts.rb"

opts = MyGetOpts.new(ARGV, {"output-prefix" => "mergedImg"})

skelHtml =<<EOS
<html>
  <head>
    <style></style>
    <link href="extra.css" type="text/css"/>
  </head>
  <body>
  </body>
</html>
EOS

objHtml = Nokogiri::HTML::Document.parse(skelHtml)
elmBODY = objHtml.css("body").last
maxWidth = nil
maxHeight = nil

while (STDIN.gets)
  toks = $_.chomp.split("\t")
  # p toks
  # p toks.collect{|t| t.length}
  elmTABLE = Nokogiri::XML::Element.new("table", objHtml)

  if ($_ =~ /^#/ && $_ !~ /^##/)
    elmTABLE_legendFirst = objHtml.css("table.legend").to_a.first

    elmContainer = Nokogiri::XML::Element.new("div", objHtml)
    elmContainer["class"] = "flex-container"
    elmBODY << elmContainer
    # p "elmContainer created for single # input line"

    elmTABLE["class"] = "legend"

    elmTHEAD = Nokogiri::XML::Element.new("thead", objHtml)
    elmTABLE << elmTHEAD
    elmTR = Nokogiri::XML::Element.new("tr", objHtml)
    elmTHEAD << elmTR
    elmTH = Nokogiri::XML::Element.new("th", objHtml)
    elmTR << elmTH
    cls = elmTABLE_legendFirst.css("thead > tr > th").to_a.first["class"]
    elmTH["class"] = cls if (cls && cls.length > 0)

    elmTBODY = Nokogiri::XML::Element.new("tbody", objHtml)
    elmTABLE << elmTBODY
    (0...elmTABLE_legendFirst.css("tbody > tr").length).each do |i|
      elmTR = Nokogiri::XML::Element.new("tr", objHtml)
      elmTBODY << elmTR
      # elmTD = Nokogiri::XML::Element.new("td", objHtml)
      # elmTR << elmTD
      # cls = elmTABLE_legendFirst.css("tbody > tr").to_a[i].css("td").to_a.first["class"]
      # elmTR["class"] = cls if (cls.length > 0)
      elmTH = Nokogiri::XML::Element.new("th", objHtml)
      elmTR << elmTH
      cls = elmTABLE_legendFirst.css("tbody > tr").to_a[i].css("th").to_a.first["class"]
      elmTH["class"] = cls if (cls && cls.length > 0)
    end

    elmContainer << elmTABLE
    next
  end

  elmTHEAD = Nokogiri::XML::Element.new("thead", objHtml)
  elmTABLE << elmTHEAD
  elmTR = Nokogiri::XML::Element.new("tr", objHtml)
  elmTHEAD << elmTR
  elmTH = Nokogiri::XML::Element.new("th", objHtml)
  elmTR << elmTH
  thead_th_text = toks.shift().gsub(/^##+/, "")
  if (opts.space_to_br)
    thead_th_text.split(" ").each_with_index do |t, i|
      if (i > 0)
        elmTH << Nokogiri::XML::Element.new("br", objHtml)
      end
      elmTH << Nokogiri::XML::Text.new(t, objHtml)
    end
  else
    elmTH << Nokogiri::XML::Text.new(thead_th_text, objHtml)
  end

  elmTBODY = Nokogiri::XML::Element.new("tbody", objHtml)
  elmTABLE << elmTBODY

  if ($_ =~ /^##+/)
    elmContainer = Nokogiri::XML::Element.new("div", objHtml)
    elmContainer["class"] = "flex-container"
    # p "elmContainer created for double # input line"
    elmBODY << elmContainer
  end

  while (imgPath = toks.shift())
    # p imgPath
    elmTR = Nokogiri::XML::Element.new("tr", objHtml)
    elmTBODY << elmTR

    if ($_ =~ /^##+/)
      elmTABLE["class"] = "legend"

      elmTH = Nokogiri::XML::Element.new("th", objHtml)
      elmTR << elmTH
      elmTH["class"] = "book-name"
      elmTH << Nokogiri::XML::Text.new(imgPath, objHtml)

    else
      elmTABLE["class"] = "pair"

      elmTD = Nokogiri::XML::Element.new("td", objHtml)
      elmTR << elmTD
      elmIMG = Nokogiri::XML::Element.new("img", objHtml)
      elmTD << elmIMG

      mgkImg = Magick::ImageList.new(imgPath).first
      h = mgkImg.rows
      w = mgkImg.columns

      if (opts.embed_img)
        imgF = File::open(imgPath, "r")
        fmt = imgPath.split(".").pop().downcase
        if (fmt == "jpg")
          fmt = "jpeg"
        end
        elmIMG["src"] = "data:image/" + fmt + ";base64," + Base64::strict_encode64(imgF.read)
        imgF.close
      else
        elmIMG["src"] = imgPath
      end
      elmIMG["alt"] = File::basename(imgPath)
      elmIMG["title"] = File::basename(imgPath)
      if (opts.height)
        elmIMG["height"] = opts.height
        elmIMG["width"] = sprintf("%3.1f", (1.0 * w / h) * opts.height)
      elsif (opts.width)
        elmIMG["height"] = sprintf("%3.1f", (1.0 * h / w) * opts.width)
        elmIMG["width"] = opts.width
      end

      if (!maxWidth || maxWidth < elmIMG["width"].to_f)
        maxWidth = elmIMG["width"].to_f.ceil.to_i
      end
      if (!maxHeight || maxHeight < elmIMG["height"].to_f)
        maxHeight = elmIMG["height"].to_f.ceil.to_i
      end
      # p [[w, h], [maxWidth, maxHeight]]


      mgkImg.destroy!
    end
  end

  elmContainer << elmTABLE
end

cssText =<<EOS

      table, thead, tbody, tr, th, td {
        border-collapse: collapse;
        border: 1px solid black;
      }
      .flex-container {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
      }
      table { margin-bottom: 10 }
      td {
        text-align: center;
        vertical-align: middle;
      }
      thead > tr > th {
        vertical-align: top;
      }
EOS
cssText += sprintf("      td { width: %d }\n", maxWidth.ceil)
cssText += sprintf("      th.book-name { width: %d }\n", maxWidth)
cssText += sprintf("      th.book-name { height: %d }\n", opts.height)
cssText += "    "
objHtml.css("style").first << Nokogiri::XML::Text.new(cssText, objHtml)

[ "jquery.min.js", "fit-legend.js"].each do |js|
  elmSCRIPT = Nokogiri::XML::Element.new("script", objHtml)
  elmBODY << elmSCRIPT
  elmBODY << Nokogiri::XML::Text.new("\n", objHtml)
  elmSCRIPT["type"] = "text/javascript"

  if (js == "jquery.min.js" && (opts.remote_js || opts.remote_jquery))
    elmSCRIPT["src"] = "https://code.jquery.com/jquery.min.js"
  elsif (opts.remote_js)
    elmSCRIPT["src"] = ("https://gl.githack.com/mpsuzuki/gen-svg-from-xlsx/-/raw/master/" + js)
  elsif (opts.embed_js)
    fh = File::open(js, "r")
    elmSCRIPT << Nokogiri::XML::CDATA.new(objHtml, "\n<!--\n" + fh.read + "\n-->")
    fh.close
  else
    elmSCRIPT["src"] = js
  end
end

legendNames = []
objHtml.css("table.legend").each do |elmTABLE|
  # p legendNames
  # p elmTABLE.css("tr").length
  elmTABLE.css("tr").to_a.each_with_index do |elmTR, i|
    if (elmTR.text.gsub(/\s/, "").length > 0)
      legendNames[i] = elmTR.children[0].children.to_xml()
    else
      # p elmTR.css("th, td").length
      # p [i, legendNames[i]]
      # elmTR.css("th, td").to_a.first << Nokogiri::XML::Text.new(legendNames[i], objHtml)
      elmTR.css("th, td").to_a.first.children = legendNames[i]
    end
  end
end

puts objHtml.to_html
