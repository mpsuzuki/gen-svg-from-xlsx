const updateMiniTableSizes = function() {
  let maxWidthTH = 0;
  let maxHeightTH = 0;
  const elmsTHEAD_TH = $("table.pair > thead > tr > th");
  console.log("found " + elmsTHEAD_TH.length + " table.pair > thead > tr > th");
  for (let i = 0; i < elmsTHEAD_TH.length; i++) {
    let w = $(elmsTHEAD_TH[i]).width();
    let h = $(elmsTHEAD_TH[i]).outerHeight();
    if (maxWidthTH < w) { maxWidthTH = w; };
    if (maxHeightTH < h) { maxHeightTH = h; };
  };
  console.log("maxWidth of table.pair > thead > th = " + maxWidthTH);
  console.log("maxHeight of table.pair > thead > th = " + maxHeightTH);
  for (let i = 0; i < elmsTHEAD_TH.length; i++) {
    // $(elmsTHEAD_TH[i]).width(maxWidthTH);
    $(elmsTHEAD_TH[i]).height(maxHeightTH);
  };

  const elmTABLE_pair = document.body.querySelector("table.pair");
  const arrJq_TH_TD = [];
  let elmsTH_TD = elmTABLE_pair.querySelectorAll("th, td");
  for (let i = 0; i < elmsTH_TD.length; i++) {
    arrJq_TH_TD.push( $(elmsTH_TD[i]) );
  };

  const elmsTABLE_legend = document.body.querySelectorAll("table.legend");
  for (let i = 0; i < elmsTABLE_legend.length; i++) {
    console.log("* fix " + i + "-th legend");
    const elmTABLE = elmsTABLE_legend[i];

    let elmsTH_TD = elmTABLE.querySelectorAll("th, td"); 
    for (let j = 0; j < elmsTH_TD.length; j++) {
      let jqTH_TD = $(elmsTH_TD[j]);
      console.log("  fix " + j + "-th cell width=" + arrJq_TH_TD[j].width() + "x" + arrJq_TH_TD[j].height());
      jqTH_TD.width(arrJq_TH_TD[j].width());
      jqTH_TD.height(arrJq_TH_TD[j].outerHeight());
    };
  };
};

$(window).resize(updateMiniTableSizes);
$(document).ready(updateMiniTableSizes);
