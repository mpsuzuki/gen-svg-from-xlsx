#!/usr/bin/env

class MyGetOpts
  attr_reader(:opts)

  def _parseStrAsNumber(str)
    vInt = nil
    begin
      vInt = Integer(str)
    rescue
      vInt = nil
    end
    return vInt if (vInt != nil)

    vFloat = nil
    begin
      vFloat = Float(str)
    rescue
      vFloat = nil
    end
    return vFloat if (vFloat != nil)

    return str
  end

  def initialize(argv, hsPreset)
    @opts = {}
    hsPreset.each do |k, v|
      @opts[k] = v
    end

    if (@opts.include?("files"))
      @opts["files"] = []
    end

    argv.each do |a|
      if (a[0..0] != "-")
        @opts["args"].push(a)
        next
      end

      if (a[0..1] == "--")
        if (a.include?("="))
          k, v = a[2..-1].split("=", 2)
          @opts[ k ] = self._parseStrAsNumber(v)
        elsif (a =~ /^--enable-/ || a =~ /^--with-/)
          k = a[2..-1].split("-")[1..-1].join("-")
          @opts[ k ] = true
        elsif (a =~ /^--disable-/ || a =~ /^--without-/)
          k = a[2..-1].split("-")[1..-1].join("-")
          @opts[ k ] = false
        else
          k = a[2..-1]
          @opts[ k ] = true
        end
      elsif (a[0..0] == "-")
        @opts[ a[1..-1] ] = true
      end
    end
  end

  def method_missing(mth, *args)
    if (mth[-1..-1] == "=")
      @opts[mth[0..-2]] = args[0]
      return self
    elsif (@opts.include?(mth))
      return @opts[mth]
    else
      hph_key = mth.to_s.gsub("_", "-")
      if (@opts.include?(hph_key))
        return @opts[hph_key]
      end

      hph_key_fallback = mth.to_s.gsub(/([^A-Z])([A-Z])/, '\1 \2').split(" ").collect{|t| t[0..0].downcase + t[1..-1]}.join("-")
      if (@opts.include?(hph_key_fallback))
        return @opts[hph_key_fallback]
      end
    end

    return nil
  end
end
