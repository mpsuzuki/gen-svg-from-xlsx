#!/usr/bin/env ruby2.5

require "nokogiri"
require "rmagick"
require "base64"
require "json"

args = ARGV.select{|a| a[0..1] != "--"}
opts = {}
opts["output-prefix"] = "margedImg"
ARGV.select{|a| a[0..1] == "--"}.each{|a|
  v = a[2..-1]
  if (!v.include?("="))
    opts[v] = true
  else
    k, v = v.split("=", 2)
    vs = nil
    if (v.include?(","))
      vs = v.split(",").collect{|a| a.length > 0}
    end

    if (opts.include?(k))
      if (opts[k].class.to_s == "Array")
        if (vs && vs.length > 0)
          opts[k] += vs
        else
          opts[k].push(v)
        end
      elsif (vs)
        opts[k] = [ opts[k] ] + vs
      else
        opts[k] = v
      end
    else
      if (vs)
        opts[k] = vs
      else
        opts[k] = v
      end
    end
  end
}

dirs = args.select{|a| File::directory?(a)}
files = args.select{|a| File::readable?(a) && !File::directory?(a)}

if (files.length == 0)
  STDERR.puts("*** at least, a pathname for drawing[0-9]+.xml in XLSX is needed")
  exit(-1)
end

def getHashFromRels(pathXmlUsingRels)
  rHash = {}
  d = File::dirname(pathXmlUsingRels)
  pathRels = [ d, "_rels", File::basename(pathXmlUsingRels) + ".rels" ].join("/")
  Nokogiri::XML(File::open(pathRels)).css("Relationship").each do |r|
    rHash[r["Id"]] = [d, r["Target"]].join("/")
  end
  return rHash
end

def getCommentHashFromWorksheet(pathWorksheetXml)
  rArray = []
  ssArray = []
  d = File::dirname(pathWorksheetXml)
  fp = [d, "..", "sharedStrings.xml"].join("/")
  Nokogiri::XML(File::open(fp, "r")).css("si").each do |elmSi|
    ssArray.push("")
    elmSi.children.each do |elm|
      if (elm.name == "t")
        ssArray.last << elm.text
      elsif (["r"].include?(elm.name))
        elm.css("rPr > t").each{|t| ssArray.last << t.text}
      end
    end
  end

  # p ssArray
  # p pathWorksheetXml

  Nokogiri::XML(File::open(pathWorksheetXml, "r")).css("sheetData > row > c").each do |elmC|
    elmRow = elmC.parent
    iRow = elmRow["r"].to_i - 1
    iColumn = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".index( elmC["r"][0..0] )

    next if (elmC.css("v").length == 0)

    dbg = [iRow, iColumn]

    t = elmC.css("v").first.text
    dbg.push(t)

    if (elmC["t"] && elmC["t"] == "s")
      t = ssArray[ t.to_i ]
      dbg.push(t)
    end

    p dbg

    rArray[iRow] = [] if (rArray[iRow] == nil)
    rArray[iRow][iColumn] = t
  end

  return rArray
end

class XdrRect
  attr_accessor(:idx_col, :idx_row)
  attr_accessor(:off_x, :off_y, :ext_cx, :ext_cy)
  attr_accessor(:clr, :alpha)

  def initialize(elmCellAnchor)
    elmXdrFrom = elmCellAnchor.css("xdr|from").first
    @idx_col = elmXdrFrom.css("xdr|col").first.text.to_i
    @idx_row = elmXdrFrom.css("xdr|row").first.text.to_i

    elmAXfrm = elmCellAnchor.css("xdr|spPr > a|xfrm").first
    elmAOff = elmAXfrm.css("a|off").first
    @off_x = elmAOff["x"].to_i
    @off_y = elmAOff["y"].to_i
    elmAExt = elmAXfrm.css("a|ext").first
    @ext_cx = elmAExt["cx"].to_i
    @ext_cy = elmAExt["cy"].to_i

    elmSrgbClr = elmCellAnchor.css("xdr|sp > xdr|spPr > a|solidFill > a|srgbClr").first
    if (elmSrgbClr)
      @clr = elmSrgbClr["val"]
      @alpha = elmSrgbClr.css("a|alpha").first["val"].to_f
    end
  end

  def inspect
    rInspect = {}
    rInspect[:cell] = [@idx_row, @idx_col]
    rInspect[:geom] = [@off_x, @off_y, @ext_cx, @ext_cy]
    rInspect[:clr]  = [@clr, @alpha]
    return rInspect.inspect
  end

  def createRowColInfoString(maxRow, maxColumn, delimit)
    lenRow = maxRow.to_s.length
    sRow = (("0" * lenRow) + @idx_row.to_s)[(0 - lenRow)..-1]

    lenColumn = maxColumn.to_s.length
    sColumn = (("0" * lenColumn) + @idx_col.to_s)[(0 - lenColumn)..-1]

    if (delimit)
      return (sRow + delimit + sColumn)
    else
      return (sRow + sColumn)
    end
  end
end

class XdrRectImg < XdrRect
  attr_accessor(:rsrcID)
  attr_accessor(:imgPathname, :ping)

  def setRsrcID(elmCellAnchor)
    @rsrcID = elmCellAnchor.css("xdr|pic > xdr|blipFill > a|blip").first["r:embed"]
    return self
  end

  def resolveRsrcID(rsrcHash)
    @imgPathname = rsrcHash[@rsrcID]
    if (File::readable?(@imgPathname))
      @ping = Magick::ImageList.new.ping(@imgPathname).first
    end
    return self
  end

  def inspect
    rInspect = {}
    rInspect[:cell] = [@idx_row, @idx_col]
    rInspect[:path] = [@rsrcID, @imgPathname]
    rInspect[:geom] = [@off_x, @off_y, @ext_cx, @ext_cy]
    rInspect[:ping] = [@ping.columns, @ping.rows]
    return rInspect.inspect
  end
end

class XdrImgWithRects
  attr_accessor(:image, :rects)

  def initialize(elm)
    @image = XdrRectImg.new(elm).setRsrcID(elm)
    @rects = []
  end

  def initialize(elm, hs)
    @image = XdrRectImg.new(elm).setRsrcID(elm).resolveRsrcID(hs)
    @rects = []
  end

  def append_rect(elm)
    @rects.push( XdrRect.new(elm) )
    return self
  end

  def idx_col
    return @image.idx_col
  end

  def idx_row
    return @image.idx_row
  end

  def inspect
    rInspect = {}
    rInspect[:img] = @image.inspect
    rInspect[:rects] = @rects.collect{|r| r.inspect}
    return rInspect.inspect
  end

  def getImgBase64()
    img = Magick::ImageList.new(@image.imgPathname).first
    if (["jpeg", "png"].include?(img.format))
      return Base64::strict_encode64(img.to_blob)
    else
      return Base64::strict_encode64(img.to_blob{self.format = "png"})
    end
  end

  def genSvg(pathSkel, pathOutput)
    svgXml = Nokogiri::XML(File::open(pathSkel))

    elmImg = svgXml.css("image").first
    if (@image.ping.format == "jpeg")
      elmImg["xlink:href"] = "data:image/jpeg;base64," + self.getImgBase64()
    else
      elmImg["xlink:href"] = "data:image/png;base64," + self.getImgBase64()
    end
    elmImg["id"] = @image.rsrcID
    elmImg["x"] = 0
    elmImg["y"] = 0
    elmImg["width"] = @image.ping.columns
    elmImg["height"] = @image.ping.rows
    svgXml.root["width"] = @image.ping.columns
    svgXml.root["height"] = @image.ping.rows

    rects.each do |rect|
      elmRect = Nokogiri::XML::Element.new("rect", svgXml)
      elmRect["x"] = (rect.off_x - @image.off_x) * @image.ping.columns / @image.ext_cx
      elmRect["y"] = (rect.off_y - @image.off_y) * @image.ping.rows / @image.ext_cy
      elmRect["width"] = rect.ext_cx * @image.ping.columns / @image.ext_cx
      elmRect["height"] = rect.ext_cy * @image.ping.rows / @image.ext_cy
      elmRect["fill"] = "#" + rect.clr
      elmRect["fill-opacity"] = rect.alpha / 100000
      svgXml.root << elmRect
    end

    outText = svgXml.to_xml(:save_with => Nokogiri::XML::Node::SaveOptions::AS_XML)
    if (pathOutput)
      File::open(pathOutput, "w"){|f| f.write(outText)}
    else
      puts outText
    end
  end
end


arr = []

drawingXML = Nokogiri::XML(File::open(files.first))
relsHash = getHashFromRels(files.first)

pathWorksheet = [File::dirname(files.first), "..", "worksheets", "sheet" + File::basename(files.first)[-5..-1]].join("/")
commentHash = getCommentHashFromWorksheet(pathWorksheet)

drawingXML.css("xdr|pic").each do |elmXdrPic|
  xdrImgWithRects = XdrImgWithRects.new( elmXdrPic.parent , relsHash)

  iColumn = xdrImgWithRects.idx_col
  iRow = xdrImgWithRects.idx_row

  arr[iRow] = [] if (arr[iRow] == nil)
  arr[iRow][iColumn] = [] if (arr[iRow][iColumn] == nil)
  if (arr[iRow][iColumn].length == 0)
    arr[iRow][iColumn].push( xdrImgWithRects )
  end
end

drawingXML.css("xdr|sp > xdr|nvSpPr").each do |elmXdrNvSpPr|
  xdrRect = XdrRect.new( elmXdrNvSpPr.parent.parent )
  iColumn = xdrRect.idx_col
  iRow = xdrRect.idx_row
  arr[iRow][iColumn].last.rects.push( xdrRect )
end

# p "dump arr"
# p arr

# puts arr.inspect
queSvg = []
maxRow = arr.length
maxColumn = arr.select{|a| a != nil}.collect{|a| a.length}.max
arr.each_with_index do |row, iRow|
  next if (!row)
  row.each_with_index do |cell, iColumn|
    next if (!cell)

    if (cell.last.rects.length > 0 || opts.include?("include-non-marked"))
      tag = nil
      if (opts.include?("pick-tag-from-column"))
        tag = "seq" + commentHash[iRow][opts["pick-tag-from-column"].to_i].split(":").first
      end

      if (tag == nil)
        svgBaseName = opts["output-prefix"] + "-" + cell.last.image.createRowColInfoString(maxRow, maxColumn, "-") + ".svg"
      elsif (opts.include?("insert-tag-before-rc"))
        svgBaseName = opts["output-prefix"] + "-" + tag + "-" + cell.last.image.createRowColInfoString(maxRow, maxColumn, "-") + ".svg"
      elsif (opts.include?("insert-tag-after-rc"))
        svgBaseName = opts["output-prefix"] + "-" + cell.last.image.createRowColInfoString(maxRow, maxColumn, "-") + "-" + tag + ".svg"
      end

      if (opts.include?("dst-dir"))
        outPath = [opts["dst-dir"], svgBaseName].join("/")
        puts outPath
        cell.last.genSvg("skel.svg", outPath)
      else
        puts svgBaseName
        cell.last.genSvg("skel.svg", nil)
      end
    end
  end
end
