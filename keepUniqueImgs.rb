#!/usr/bin/env ruby2.5

require "nokogiri"

arr = []


args = ARGV.select{|a| a[0..1] != "--"}
opts = ARGV.select{|a| a[0..1] == "--"}.collect{|a| a[2..-1]}
pathSrc = args.first
if (pathSrc)
  drawingXML = Nokogiri::XML(File::open(pathSrc))
else
  drawingXML = Nokogiri::XML(STDIN)
end

drawingXML.css("xdr|pic").each do |elmXdrPic|
  embedRsrcID = elmXdrPic.css("xdr|blipFill > a|blip").first["r:embed"]

  elmXdrCellAnchor = elmXdrPic.parent
  elmXdrFrom = elmXdrCellAnchor.css("xdr|from")
  elmXdrTo = elmXdrCellAnchor.css("xdr|to")

  iColumn = elmXdrFrom.css("xdr|col").first.text.to_i
  iRow = elmXdrFrom.css("xdr|row").first.text.to_i

  arr[iRow] = [] if (arr[iRow] == nil)
  arr[iRow][iColumn] = {} if (arr[iRow][iColumn] == nil)
  if (opts.include?("only-one"))
    if (arr[iRow][iColumn].keys.length > 0)
      # STDERR.printf("skip " + embedRsrcID + " to " + iRow.to_s + "," + iColumn.to_s + "\n")
      elmXdrCellAnchor.remove()
      next
    end
  end
  arr[iRow][iColumn][embedRsrcID] = [] if (arr[iRow][iColumn][embedRsrcID] == nil)
  arr[iRow][iColumn][embedRsrcID].push(elmXdrCellAnchor)
  # STDERR.printf("push " + embedRsrcID + " to " + iRow.to_s + "," + iColumn.to_s + "\n")
  if (arr[iRow][iColumn][embedRsrcID].length > 1)
    if (opts.include?("keep-last"))
      arr[iRow][iColumn][embedRsrcID].shift().remove()
      # STDERR.printf("shift " + embedRsrcID + " to " + iRow.to_s + "," + iColumn.to_s + "\n")
    else
      arr[iRow][iColumn][embedRsrcID].pop().remove()
      # STDERR.printf("pop " + embedRsrcID + " to " + iRow.to_s + "," + iColumn.to_s + "\n")
    end
  end
end

if (opts.include?("pretty"))
  outText = drawingXML.to_xml
else
  outText = drawingXML.to_xml(:save_with => Nokogiri::XML::Node::SaveOptions::AS_XML)
end

if (args.length < 2)
  puts outText
else
  File::open(args[1]){|f| f.write(outText)}
end
