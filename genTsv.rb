#!/usr/bin/env ruby2
Encoding.default_internal = "utf-8"
Encoding.default_external = "utf-8"

require "./myGetOpts.rb"
opts = MyGetOpts.new(ARGV, {"suffix" => "png"})

if (opts.dir)
  imgs = Dir.glob(opts.dir + "/*." + opts.suffix).collect{|t| t.gsub("//", "/")}
else
  imgs = opts.files
end

seqs = Hash.new
imgs.each do |path|
  toks = File::basename(path, ".png").split("-")
  aSeq = toks[1].gsub("seq", "")
  if !seqs.include?(aSeq)
    seqs[aSeq] = Array.new
  end
  seqs[aSeq].push(path)
end

puts [
  "##seq ucs",
  "\u{85E4}",
  "\u{9673}"
].join("\t")

STDIN.read.split("\r\n").each do |aLine|
  toks = aLine.split("\t")

  if(toks.first =~ /^#/)
    puts("#")
  elsif (toks.first =~ /^[0-9]{5}:/)
    seq = toks[0].split(":").first
    loc = toks[1]
    ucsStr = toks[2]
    ucsHex = toks[3]

    if (ucsStr[0..0] == "\"")
      ucsStr = ucsStr[1..-2]
    end

    ucsStr = ucsStr.split(",").first

    puts [
      [seq, ucsStr].join(" "),
      seqs[seq].sort.join("\t")
    ].join("\t")
  end
end
