#!/usr/bin/env ruby2.5

require "nokogiri"

arr = []


args = ARGV.select{|a| a[0..1] != "--"}
opts = ARGV.select{|a| a[0..1] == "--"}.collect{|a| a[2..-1]}
pathSrc = args.first
if (pathSrc)
  drawingXML = Nokogiri::XML(File::open(pathSrc))
else
  drawingXML = Nokogiri::XML(STDIN)
end

# drawingXML.css("xdr|wsDr > xdr|twoCellAnchor > xdr|pic").each do |elmXdrPic|
drawingXML.css("xdr|pic").each do |elmXdrPic|
  embedRsrcID = elmXdrPic.css("xdr|blipFill > a|blip").first["r:embed"]

  elmXdrCellAnchor = elmXdrPic.parent
  elmXdrFrom = elmXdrCellAnchor.css("xdr|from")
  elmXdrTo = elmXdrCellAnchor.css("xdr|to")

  iColumn = elmXdrFrom.css("xdr|col").first.text.to_i
  iRow = elmXdrFrom.css("xdr|row").first.text.to_i

  arr[iRow] = [] if (arr[iRow] == nil)
  arr[iRow][iColumn] = [] if (arr[iRow][iColumn] == nil)
  arr[iRow][iColumn].push(embedRsrcID)
end

arr.each_with_index do |row, iRow|
  if (row && row.length > 0)
    row.each_with_index do |cell, iColumn|
      if (cell && cell.length > 0)
        if (opts.include?("no-single") && cell.length > 1)
          p [iRow, iColumn, cell]
        elsif (!opts.include?("no-single"))
          p [iRow, iColumn, cell]
        end
      end
    end
  end
end
